FROM python:3
WORKDIR /usr/src/app
COPY ./requirements.txt ./
COPY ./launch.sh ./
RUN pip3 install --no-cache-dir -r requirements.txt
RUN chmod a+x launch.sh
COPY src .
EXPOSE 8000
#CMD [ "python", "./back_end/soap_srv.py", "&" ]
#CMD [ "python", "./dj_website/manage.py", "runserver", "0.0.0.0:8000" ]
CMD ["./launch.sh"]