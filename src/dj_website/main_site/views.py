from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .models import *
from .forms import *
from geopy.geocoders import Nominatim
from zeep import Client
from .functions import *
import io


def index(request):
    all_cars = Vehicle.objects.all()
    template="travel.html"
    context = {
        "test": "This is a test",
        "all_cars": all_cars
    }
    return render(request, template, context)

def about(request):
    template="about.html"
    return render(request, template)
    
def calculateTrip(request):
    if request.method == 'POST':
        form = TravelForm(request.POST)
        if form.is_valid():

            # Getting car objects from the database
            car = get_object_or_404(Vehicle, pk=form.cleaned_data['selected_car'])
            
            # Getting travel and recharge infos using SOAP api
            client = Client('http://127.0.0.1:8080/?WSDL')
            try:
                result = client.service.infos( form.cleaned_data['departure'], form.cleaned_data['destination'], car.mileage)
            except Exception as e:
                print(e)
                template = "incorrect_city.html"
                return render(request, template)
            
            # Extracting the coordinates for each stops and formatting the travel info for web display
            formated_result = formatString(result)
            print(formated_result)
            stopcoords = []
            banned_numbers = ""
            stops = formated_result.get("stops")
            if stops is not None:
                for stop in stops:
                    if "à" in stop.get("stop"):
                        if str(stop.get("number")) not in banned_numbers:
                            stopcoords.append(extractCoords(stop.get("stop")))
                            banned_numbers += str(stop.get("number"))
                        stop["stop"] = stop.get("stop").split("|")[0]

            ### Defining the context of the webpage
            template="result.html"
            
            # In case of invalid address, adressToCoords will return a None object
            # We want to redirect to an error page in those cases
            if addressToCoords(form.cleaned_data['departure']) is None or addressToCoords(form.cleaned_data['destination']) is None:
                template = "incorrect_city.html"
                return render(request, template)
            else:
                context = {
                    "result": formated_result,
                    "destination": form.cleaned_data['destination'],
                    "departure": form.cleaned_data['departure'],
                    "coos_dep" : { "lat" : addressToCoords(form.cleaned_data['departure'])[0], "long": addressToCoords(form.cleaned_data['departure'])[1]},
                    "coos_dest" : { "lat" : addressToCoords(form.cleaned_data['destination'])[0], "long": addressToCoords(form.cleaned_data['destination'])[1] },
                    "stopcoords" : stopcoords
                }
                print(context)
                return render(request, template, context)

    return HttpResponseRedirect('/')
