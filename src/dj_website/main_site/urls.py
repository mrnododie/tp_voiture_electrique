from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('calculate', views.calculateTrip, name='calculate'),
    path('about', views.about, name="about")
    # path('result', views.result, name='result'),
]