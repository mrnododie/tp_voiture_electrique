import io
from geopy.geocoders import Nominatim

def cleanAddress(address):
    if "Pour l'arret" in address :
        return ""
    elements = address.split(" ")
    result = ""
    for element in elements :
        if element == "à":
            break
        else :
            result += element + " "
    return result.replace(",", "").replace("'"," ").replace(".", "")


def googleMapsFromAddress(address):
    # Generates a google map url pointing to the address
    # WARNING it must be cleaned by cleanAddress before
    if address != "":
        return "https://www.google.com/maps/search/?api=1&query="+address.replace(" ", "+")[:-1]
    else :
        return ""

def compactStops(stops):
    compacted = []
    for i in range(len(stops)-2):
        if not stops[i].get('stop').lower()[0:16] == stops[i+1].get('stop').lower()[0:16]:
            compacted.append(stops[i])
    return compacted

def addressToCoords(address):
    locator = Nominatim(user_agent="myGeocoder")
    location = locator.geocode(address)
    if location is None :
        return None
    else:
        return [location.latitude, location.longitude]

# Formats the string returned by to the API to a dictionnary that's
# easier to enumerate and manipulate
def formatString(travel):
    if "arret" in travel :
        # Il y a plusieurs arrêts
        # Coupage du string en lignes
        buffer = io.StringIO(travel)
        nbofstops = 0
        distance = 0
        stops = []
        for line in buffer :
            if "Distance a parcourir" in line :
                distance = line.split(":")[1]
            elif "arret" in line:
                nbofstops +=1
                stops.append({'number' : nbofstops, 'stop' : line})
            elif line == '\n':
                print("empty line")
            else:
                stops.append({'number' : nbofstops, 'stop' : line})
        format =  {
            'has_stops' : True,
            'nbofstops' : nbofstops,
            'distance' : distance,
            'stops' : compactStops(stops)
        }
        for stop in format.get('stops'):
                stop['gmaplink'] = googleMapsFromAddress(cleanAddress(stop.get('stop')))
                stop['clean_address'] = cleanAddress(stop.get('stop'))
    else :
        buffer = io.StringIO(travel)
        distance = 0
        for line in buffer:
            if "Distance a parcourir" in line :
                distance = line.split(":")[1]
        # Le trajet est direct
        format = {
            "has_stops" : False,
            "distance" : distance
        }
    return format

def extractCoords(line):
    coords = line.split("|")[1]
    splitcoords = { "lat" : coords.split(",")[0], "long" : coords.split(",")[1] }
    return splitcoords
