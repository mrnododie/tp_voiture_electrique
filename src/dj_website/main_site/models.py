from django.db import models

# Create your models here.


class Vehicle(models.Model):
    car_make = models.CharField(max_length=200)
    car_model = models.CharField(max_length=200)
    charging_time = models.CharField(max_length=200)
    mileage = models.IntegerField()
    
    def __str__(self):
        return str(self.car_make) + " " + str(self.car_model) + " (" + str(self.mileage)+" km)"
    