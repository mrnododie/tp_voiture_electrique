from django import forms

class TravelForm(forms.Form):
    departure = forms.CharField(label='departure', max_length=512)
    destination = forms.CharField(label='destination', max_length=512)
    selected_car = forms.IntegerField()