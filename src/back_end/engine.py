import urllib.request, json
from geopy.geocoders import Nominatim
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
#from sqlalchemy.sql.base import ColumnCollection
#from .models import *

# https://opendata.reseaux-energies.fr/explore/dataset/bornes-irve/api/?disjunctive.region&geofilter.distance=48.8520930694,2.34738897685,100

# Fonction principale appelé par le serveur SOAP.
def travel(cityDep, cityArr, car):
    locationDep = cityToCoord(cityDep)
    locationArr = cityToCoord(cityArr)
    coord = str(locationDep.latitude)+","+str(locationDep.longitude)+";"+str(locationArr.latitude)+","+str(locationArr.longitude)

    urlTravel = "http://router.project-osrm.org/route/v1/driving/"+coord+"?overview=false"
    with urllib.request.urlopen(urlTravel) as url:
        data = json.loads(url.read().decode())

    for items in data["routes"]:
        for elements in items["legs"]:
            distance = elements["distance"]
    distance = distance/1000

    listStop = needBreak(locationDep.latitude, locationDep.longitude, locationArr.latitude, locationArr.longitude, distance, car)

    print("\n\n"+str(listStop))
    return "\n\nDistance a parcourir :"+str(distance)+"\n\n"+str(listStop)

# Permet de "traduire" une ville en coordonnées gps
def cityToCoord(city):
    geolocator = Nominatim(user_agent="LOL")
    coord = geolocator.geocode(city)
    print(coord, coord.latitude, coord.longitude)
    return coord

# Calcule le nombre d'arrets obligatoire pour charger
def needBreak(latDep, longDep, latArr, longArr, dist, car):
    autonomy, lowbatterie = reserve(car)
    if dist < autonomy:
        return 0
    else:
        nrbBreak = round(dist/autonomy)
        latDiff = latDep - latArr
        longDiff = longDep - longArr

        latDiff = latDiff / nrbBreak
        longDiff = longDiff / nrbBreak

        nbr = 0
        listStop = ""
        while nrbBreak != 0:
            nbr += 1
            if latDep < latArr:
                latDep = latDep + abs(latDiff)
                positionLat = 1
            else: 
                latDep = latDep - latDiff
                positionLat = 0
            if longDep > longArr:
                longDep = longDep + (longDiff*-1)
                positionLon = 1
            else:
                longDep = longDep - longDiff
                positionLon = 0
            print("\n\n coor stop:"+str(latDep), str(longDep))
            #print("########## \n\n\n"+str(latDep)+"\n"+str(longDep)+"\n\n\n##########\n")
            listStop += "Pour l'arret n°"+str(nbr)+": \n\n"+str(nearBornes (latDep, longDep, lowbatterie, positionLat, positionLon, latDiff, longDiff))+"\n\n"
            nrbBreak = nrbBreak - 1
        return listStop

# Définit une marge à 20% de l'autonomie de la voiture.
def reserve(car):
    autonomy = car * 0.80
    if autonomy < 20:
        autonomy = car - 5
        lowbatterie = 5
    lowbatterie = (car * 0.2)*1000

    return autonomy, lowbatterie


def nearBornes(lat, long, peri, positionLat, positionLon, latDiff, longDiff):    
    # Récupération du json avec les params.
    with urllib.request.urlopen("https://opendata.reseaux-energies.fr/api/records/1.0/search/?dataset=bornes-irve&q=&facet=region&geofilter.distance="+str(lat)+"%2C"+str(long)+"%2C"+str(peri)+"") as url:
        data = json.loads(url.read().decode())
    
   # Vérification de la présence de bornes dans le périmetre.
    if data["nhits"] == 0:
        if positionLon == 1:
            if positionLat == 1:
                lat = lat - latDiff*0.7
                long = long - longDiff*0.7
            else:
                lat = lat + latDiff*0.7
                long = long - longDiff*0.7
        else:
            if positionLat == 1:
                lat = lat - latDiff*0.7
                long = long + longDiff*0.7
            else:
                lat = lat + latDiff*0.7
                long = long + longDiff*0.7

        latDiff = latDiff*0.5
        longDiff = longDiff*0.5

        nearBornes(lat, long, peri, positionLat, positionLon, latDiff, longDiff)
    else:
        infos = parsage(data)
        
    return infos
    

def parsage(data):
    infos = ""
    for items in data["records"]:
        infos += str(items["fields"]["ad_station"])+" à "+str(round(float(items["fields"]["dist"])))+"m. | "+str(items["fields"]["ylatitude"])+","+str(items["fields"]["xlongitude"])+"\n"

    return infos

    


travel("chambéry", "grenoble", 250)